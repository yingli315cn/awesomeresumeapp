package com.jiuzhang.guojing.awesomeresume;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.jiuzhang.guojing.awesomeresume.model.BasicInfo;
import com.jiuzhang.guojing.awesomeresume.model.Education;
import com.jiuzhang.guojing.awesomeresume.model.Experience;
import com.jiuzhang.guojing.awesomeresume.util.DateUtils;
import com.jiuzhang.guojing.awesomeresume.util.ImageUtils;
import com.jiuzhang.guojing.awesomeresume.util.ModelUtils;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("ConstantConditions")
public class MainActivity extends AppCompatActivity {

    private static final int REQ_CODE_EDIT_BASIC_INFO = 100;
    private static final int REQ_CODE_EDIT_EDUCATION = 101;
    private static final int REQ_CODE_EDIT_EXPERIENCE = 102;

    private static final String MODEL_BASIC_INFO = "basic_info";
    private static final String MODEL_EDUCATIONS = "education";
    private static final String MODEL_EXPERIENCES = "experience";

    private BasicInfo basicInfo;
    private List<Education> educations;
    private List<Experience> experiences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadData();
        setupUI();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case REQ_CODE_EDIT_BASIC_INFO:
                    BasicInfo basicInfo = data.getParcelableExtra(BasicInfoEditActivity.KEY_BASIC_INFO);
                    updateBasicInfo(basicInfo);
                    break;
                case REQ_CODE_EDIT_EDUCATION:
                    String educationId = data.getStringExtra(EducationEditActivity.KEY_EDUCATION_ID);
                    if(educationId != null){
                        deleteEducation(educationId);
                    }else{
                        Education education = data.getParcelableExtra(EducationEditActivity.KEY_EDUCATION);
                        updateEducation(education);
                    }
                    break;
                case REQ_CODE_EDIT_EXPERIENCE:
                    String experienceId = data.getParcelableExtra(ExperienceEditActivity.KEY_EXPERIENCE_ID);
                    if(experienceId != null){
                        deleteExperience(experienceId);
                    }else{
                        Experience experience = data.getParcelableExtra(ExperienceEditActivity.KEY_EXPERIENCE);
                        updateExperience(experience);
                    }
                    break;
            }

        }
    }


    private void setupUI() {
        setContentView(R.layout.activity_main);
        setupBasicInfoUI();
        setupEducationsUI();
        setupExperiencesUI();
        ImageButton addEducationButton = (ImageButton)findViewById(R.id.add_education_btn);
        addEducationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(MainActivity.this, EducationEditActivity.class);
                startActivityForResult(intent, REQ_CODE_EDIT_EDUCATION);
            }
        });

        ImageButton addExperienceButton = (ImageButton) findViewById(R.id.add_experience_btn);
        addExperienceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ExperienceEditActivity.class);
                startActivityForResult(intent, REQ_CODE_EDIT_EXPERIENCE);
            }
        });
    }

    private void setupBasicInfoUI() {
        ((TextView) findViewById(R.id.name)).setText(TextUtils.isEmpty(basicInfo.name)
                                                                        ? "Your name"
                                                                        : basicInfo.name);
        ((TextView) findViewById(R.id.email)).setText(TextUtils.isEmpty(basicInfo.email)
                                                                        ? "Your email"
                                                                        : basicInfo.email);
        ImageView userPicture = (ImageView) findViewById(R.id.user_picture);
        if(basicInfo.imageUri != null){
            ImageUtils.loadImage(this, basicInfo.imageUri, userPicture);
        }else{
            userPicture.setImageResource(R.drawable.user_ghost);
        }

        ImageButton editBasicInfoBtn = (ImageButton) findViewById(R.id.edit_basic_info);

        editBasicInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BasicInfoEditActivity.class);
                intent.putExtra(BasicInfoEditActivity.KEY_BASIC_INFO, basicInfo);
                startActivityForResult(intent, REQ_CODE_EDIT_BASIC_INFO);
            }
        });
    }

    private void setupEducationsUI() {
        LinearLayout educationsLayout = (LinearLayout) findViewById(R.id.educations);
        educationsLayout.removeAllViews();
        for(Education education: educations){
            View educationView = getLayoutInflater().inflate(R.layout.education_item, null);
            setupEducationUI(educationView, education);
            educationsLayout.addView(educationView);
        }
    }

    private void setupEducationUI(View educationView, final Education education){
        String dataString = DateUtils.dateToString(education.startDate)+"~"
                +DateUtils.dateToString(education.endDate);
        ((TextView)educationView.findViewById(R.id.education_school))
                .setText(education.school+" ("+dataString+")");
        ((TextView)educationView.findViewById(R.id.education_major))
                .setText(education.major);
        ((TextView)educationView.findViewById(R.id.education_courses))
                .setText(formatItems(education.courses));

        ImageButton editEducationBtn = (ImageButton) educationView.findViewById(R.id.edit_education_btn);
        editEducationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EducationEditActivity.class);
                intent.putExtra(EducationEditActivity.KEY_EDUCATION, education);
                startActivityForResult(intent, REQ_CODE_EDIT_EDUCATION);
            }
        });

    }

    private void setupExperiencesUI(){
        LinearLayout experiencesLinearLayout = (LinearLayout) findViewById(R.id.experiences);
        experiencesLinearLayout.removeAllViews();
        for(Experience experience: experiences){
            View experienceView = getLayoutInflater().inflate(R.layout.experience_item, null);
            setupExperienceUI(experienceView, experience);
            experiencesLinearLayout.addView(experienceView);
        }
    }

    private void setupExperienceUI(View experienceView, final Experience experience){
        String dataString = DateUtils.dateToString(experience.startDate)+"~"
                +DateUtils.dateToString(experience.endDate);

        ((TextView) experienceView.findViewById(R.id.experience_company)).setText(experience.company +" ("+dataString+")");

        ((TextView) experienceView.findViewById(R.id.experience_title)).setText(experience.title);
        ((TextView) experienceView.findViewById(R.id.experience_details))
                .setText(formatItems(experience.details));

        ImageButton editExperienceBtn = (ImageButton) experienceView.findViewById(R.id.edit_experience_btn);
        editExperienceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ExperienceEditActivity.class);
                intent.putExtra(ExperienceEditActivity.KEY_EXPERIENCE, experience);
                startActivityForResult(intent, REQ_CODE_EDIT_EXPERIENCE);
            }
        });
    }

    private void fakeData() {
        basicInfo = new BasicInfo();
        basicInfo.name = "Jing Guo";
        basicInfo.email = "guojing@jiuzhang.com";

        educations = new ArrayList<>();
        Education education1 = new Education();
        education1.school = "THU";
        education1.major = "Computer Science";
        education1.startDate = DateUtils.stringToDate("09/2013");
        education1.endDate = DateUtils.stringToDate("12/2015");
        education1.courses = new ArrayList<>();
        education1.courses.add("Database");
        education1.courses.add("Algorithms");
        education1.courses.add("Networks");
        educations.add(education1);

        Education education2 = new Education();
        education2.school = "CMU";
        education2.major = "Computer Engineering";
        education2.startDate = DateUtils.stringToDate("04/2015");
        education2.endDate = DateUtils.stringToDate("11/2016");
        education2.courses = new ArrayList<>();
        education2.courses.add("Android");
        education2.courses.add("Java");
        education2.courses.add("C++");
        educations.add(education2);
    }

    private void updateBasicInfo(BasicInfo basicInfo){
        ModelUtils.save(this, MODEL_BASIC_INFO, basicInfo);
        this.basicInfo = basicInfo;
        setupBasicInfoUI();
    }

    private void updateEducation(Education education){
        boolean found = false;
        for(int i=0; i<educations.size(); i++){
            Education  e = educations.get(i);
            if(TextUtils.equals(e.id, education.id)){
                found = true;
                educations.set(i, education);
                break;
            }
        }
        if(found == false){
            educations.add(education);
        }

        ModelUtils.save(this, MODEL_EDUCATIONS, educations);
        setupEducationsUI();
    }

    private void updateExperience(Experience experience){
        boolean found = false;
        for(int i=0; i<experiences.size(); i++){
            Experience  e = experiences.get(i);
            if(TextUtils.equals(e.id, experience.id)){
                found = true;
                experiences.set(i, experience);
                break;
            }
        }
        if(found == false){
            experiences.add(experience);
        }

        ModelUtils.save(this, MODEL_EXPERIENCES, experiences);
        setupExperiencesUI();
    }


    private void deleteEducation(@Nullable String educationId){
        for(int i=0; i < educations.size(); i++){
            Education e = educations.get(i);
            if(TextUtils.equals(e.id, educationId)){
                educations.remove(i);
                break;
            }
        }

        ModelUtils.save(this, MODEL_EDUCATIONS, educations);
        setupEducationsUI();
    }

    private void deleteExperience(@Nullable String experienceId){
        for(int i = 0; i < experiences.size(); i++){
            Experience e = experiences.get(i);
            if(TextUtils.equals(e.id, experienceId)){
                experiences.remove(i);
                break;
            }
        }

        ModelUtils.save(this, MODEL_EXPERIENCES, experiences);
        setupExperiencesUI();
    }

    public static String formatItems(List<String> items) {
        StringBuilder sb = new StringBuilder();
        for (String item: items) {
            sb.append(' ').append('-').append(' ').append(item).append('\n');
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    private void loadData(){

        BasicInfo saveBasicInfo = ModelUtils.read(this, MODEL_BASIC_INFO, new TypeToken<BasicInfo>(){});
        basicInfo = saveBasicInfo == null? new BasicInfo() : saveBasicInfo;

        List<Education> savedEducation =
                ModelUtils.read(this, MODEL_EDUCATIONS, new TypeToken<List<Education>>(){});
        educations = savedEducation == null? new ArrayList<Education>():savedEducation;

        List<Experience> savedExperience =
                ModelUtils.read(this, MODEL_EXPERIENCES, new TypeToken<List<Experience>>(){});
        experiences = savedExperience == null? new ArrayList<Experience>():savedExperience;
    }

}
